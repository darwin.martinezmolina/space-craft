#!/bin/bash
source ./setenv.sh

set -e

# backend
function build_be() {
  echo -e "Building Docker Image..."
  cd ..
  docker build -t $DOCKER_IMAGE_NAME .
  docker run --name $DOCKER_IMAGE_NAME --env=H2DB_USER=$H2DB_USER --env=H2DB_PASSWORD=$H2DB_PASSWORD -p $PORT:8080 -d $DOCKER_IMAGE_NAME:latest
  echo -e "Docker image done"
}

build_be