# Stage 1: Build the application
FROM maven:3-eclipse-temurin-22-alpine AS build

# Set the working directory inside the container
WORKDIR /app

# Copy the Maven configuration file (pom.xml)
COPY pom.xml .

# Download project dependencies (without compiling the code yet)
RUN mvn dependency:go-offline -B

# Copy the project source code into the container
COPY src ./src

# Compile the application
RUN mvn clean package -DskipTests

# Stage 2: Create the final image to run the application
FROM openjdk:22-jdk-slim

# Create a directory for the application
WORKDIR /app

# Copy the compiled JAR file from the build stage
COPY --from=build /app/target/*.jar ./app.jar

# Set the port the application will run on (optional)
EXPOSE 8080

# Command to run the application
ENTRYPOINT ["java", "-jar", "app.jar"]
