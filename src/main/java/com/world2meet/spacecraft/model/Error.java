package com.world2meet.spacecraft.model;

import lombok.*;
import org.springframework.stereotype.Service;

@Getter
@Service
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@Setter
public class Error {

    int statusCode;
    String message;
    String error;
}
