package com.world2meet.spacecraft.entities;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name="spaceCrafts")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SpaceCraft {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="name", unique=true)
    private String name;

    @Column(name="description")
    private String description;
}
