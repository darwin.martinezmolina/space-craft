package com.world2meet.spacecraft.repository;

import com.world2meet.spacecraft.entities.SpaceCraft;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpaceCraftRepository extends JpaRepository<SpaceCraft, Long> {

    Page<SpaceCraft> findByNameContainingIgnoreCase(String name, Pageable pageable);
}
