package com.world2meet.spacecraft.controller;

import com.world2meet.spacecraft.entities.SpaceCraft;
import com.world2meet.spacecraft.service.SpaceCraftService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SpaceCraft entities.
 */
@RestController
@Slf4j
@RequestMapping(value="${api.prefix}")
public class SpaceCraftController {

    @Autowired
    SpaceCraftService service;

    /**
     * Retrieves a paginated list of all SpaceCraft entities.
     *
     * @param pageNumber the page number to retrieve, defaults to 1 if not provided
     * @param pageSize the number of items per page, defaults to 5 if not provided
     * @return a ResponseEntity containing the list of SpaceCraft entities and HTTP status 200 (OK)
     */
    @GetMapping()
    public ResponseEntity findByAll(@RequestParam(value="page", defaultValue = "1") int pageNumber,
                                    @RequestParam(value="size", defaultValue = "5") int pageSize){
        log.info("*** ENTER: Request to SpaceCraft: findByAll ****");

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        List<SpaceCraft> spaceCraftDB = service.findAll(pageable);
        ResponseEntity response = ResponseEntity.status(HttpStatus.OK).body(spaceCraftDB);
        log.info("*** LEAVE: Response from SpaceCraft: findByAll {}", response);
        return response;
    }

    /**
     * Retrieves a SpaceCraft entity by its ID.
     *
     * @param id the ID of the SpaceCraft to retrieve
     * @return a ResponseEntity containing the SpaceCraft entity (if found) and HTTP status 200 (OK)
     */
    @GetMapping(value="{id}")
    public ResponseEntity findById(@PathVariable long id){
        log.info("*** ENTER: Request to SpaceCraft: findById ****");

        Optional<SpaceCraft> spaceCraftDB = service.findById(id);
        ResponseEntity response = ResponseEntity.status(HttpStatus.OK).body(spaceCraftDB);
        log.info("*** LEAVE: Response from SpaceCraft: findById {}", response);
        return response;
    }

    /**
     * Retrieves a paginated list of SpaceCraft entities whose names contain the specified string (case-insensitive).
     *
     * @param name the name substring to search for
     * @param pageNumber the page number to retrieve, defaults to 1 if not provided
     * @param pageSize the number of items per page, defaults to 5 if not provided
     * @return a ResponseEntity containing the list of SpaceCraft entities and HTTP status 200 (OK)
     */
    @GetMapping(value="name/{name}")
    public ResponseEntity findByName(@PathVariable String name,
                                     @RequestParam(value="page", defaultValue = "1") int pageNumber,
                                     @RequestParam(value="size", defaultValue = "5") int pageSize){
        log.info("*** ENTER: Request to SpaceCraft: findByName ****");
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        List<SpaceCraft> spaceCraftDB = service.findByNameContainingIgnoreCase(name, pageable);
        ResponseEntity response = ResponseEntity.status(HttpStatus.OK).body(spaceCraftDB);
        log.info("*** LEAVE: Response from SpaceCraft: findByName {}", response);
        return response;
    }

    /**
     * Creates a new SpaceCraft entity.
     *
     * @param spaceCraft the SpaceCraft entity to create
     * @return a ResponseEntity containing the created SpaceCraft entity and HTTP status 201 (Created)
     */
    @PostMapping
    public ResponseEntity create(@RequestBody SpaceCraft spaceCraft){
        log.info("*** ENTER: Request to SpaceCraft: create ****");

        SpaceCraft spaceCraftDB = service.save(spaceCraft);
        ResponseEntity response = ResponseEntity.status(HttpStatus.CREATED).body(spaceCraftDB);

        log.info("*** LEAVE: Response from SpaceCraft: create {}", response);
        return response;
    }

    /**
     * Deletes a SpaceCraft entity by its ID.
     *
     * @param id the ID of the SpaceCraft to delete
     * @return a ResponseEntity with HTTP status 201 (Created) indicating the entity was deleted
     */
    @DeleteMapping(value="{id}")
    public ResponseEntity remove(@PathVariable long id){
        log.info("*** ENTER: Request to SpaceCraft: remove ****");

        service.remove(id);
        ResponseEntity response = ResponseEntity.status(HttpStatus.CREATED).body(null);

        log.info("*** LEAVE: Response from SpaceCraft: remove {}", response);
        return response;
    }

    /**
     * Updates an existing SpaceCraft entity.
     *
     * @param spaceCraft the SpaceCraft entity with updated information
     * @return a ResponseEntity containing the updated SpaceCraft entity and HTTP status 201 (Created)
     */
    @PutMapping
    public ResponseEntity update(@RequestBody SpaceCraft spaceCraft){
        log.info("*** ENTER: Request to SpaceCraft: remove ****");

        SpaceCraft spaceCraftDB = service.update(spaceCraft);
        ResponseEntity response = ResponseEntity.status(HttpStatus.CREATED).body(spaceCraftDB);

        log.info("*** LEAVE: Response from SpaceCraft: update {}", response);
        return response;
    }
}
