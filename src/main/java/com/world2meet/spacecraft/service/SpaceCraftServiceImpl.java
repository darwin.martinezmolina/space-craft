package com.world2meet.spacecraft.service;

import com.world2meet.spacecraft.entities.SpaceCraft;
import com.world2meet.spacecraft.exceptions.SpaceCraftEntityNotFoundException;
import com.world2meet.spacecraft.repository.SpaceCraftRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Implementation of the SpaceCraftService interface for managing SpaceCraft entities.
 */
@Service
@Slf4j
public class SpaceCraftServiceImpl implements SpaceCraftService {

    @Autowired
    SpaceCraftRepository repository;

    /**
     * Saves a SpaceCraft entity to the repository.
     *
     * @param spaceCraft the SpaceCraft entity to save
     * @return the saved SpaceCraft entity
     */
    @Override
    public SpaceCraft save(SpaceCraft spaceCraft) {
        log.info("  - SpaceCraftService.save - SpaceCraft {}", spaceCraft);
        return repository.save(spaceCraft);
    }

    /**
     * Retrieves all SpaceCraft entities with pagination.
     *
     * @param pageable pagination information
     * @return a list of SpaceCraft entities
     */
    @Override
    public List<SpaceCraft> findAll(Pageable pageable) {
        log.info("  - SpaceCraftService.findAll - Pageable {}", pageable);
        Page<SpaceCraft> req = repository.findAll(pageable);
        List<SpaceCraft> spaceCrafts =req.getContent();
        return spaceCrafts;
    }

    /**
     * Retrieves a SpaceCraft entity by its ID.
     *
     * @param id the ID of the SpaceCraft entity to retrieve
     * @return an Optional containing the SpaceCraft entity if found
     * @throws SpaceCraftEntityNotFoundException if the SpaceCraft entity is not found
     */
    @Override
    public Optional<SpaceCraft> findById(long id) {
        log.info("  - SpaceCraftService.findById - id {}", id);
        return Optional.ofNullable(repository.findById(id)
                .orElseThrow(() -> new SpaceCraftEntityNotFoundException("SpaceCraft", "id", String.valueOf(id))));
    }

    /**
     * Retrieves SpaceCraft entities whose names contain the specified string (case-insensitive) with pagination.
     *
     * @param name the name substring to search for
     * @param pageable pagination information
     * @return a list of SpaceCraft entities
     */
    @Override
    public List<SpaceCraft> findByNameContainingIgnoreCase(String name, Pageable pageable) {
        log.info("  - SpaceCraftService.findByNameContainingIgnoreCase - name {}, Pageable {}", name, pageable);
        Page<SpaceCraft> req = repository.findByNameContainingIgnoreCase(name, pageable);
        List<SpaceCraft> spaceCrafts =req.getContent();
        return spaceCrafts;
    }

    /**
     * Removes a SpaceCraft entity by its ID.
     *
     * @param id the ID of the SpaceCraft entity to remove
     */
    @Override
    public void remove(long id) {
        log.info("  - SpaceCraftService.remove - id {}", id);
        Optional<SpaceCraft> spaceCraftDB = findById(id);
        if(Objects.nonNull(spaceCraftDB)){
            repository.deleteById(id);
        }
    }

    /**
     * Updates a SpaceCraft entity.
     *
     * @param spaceCraft the SpaceCraft entity with updated information
     * @return the updated SpaceCraft entity
     */
    @Override
    public SpaceCraft update(SpaceCraft spaceCraft) {
        log.info("  - SpaceCraftService.update - SpaceCraft {}", spaceCraft);
        Optional<SpaceCraft> spaceCraftDB = findById(spaceCraft.getId());
        if(Objects.nonNull(spaceCraftDB)){
            spaceCraft.setId(spaceCraftDB.get().getId());
            return repository.save(spaceCraft);
        }
        return repository.save(spaceCraft);
    }
}
