package com.world2meet.spacecraft.service;

import com.world2meet.spacecraft.entities.SpaceCraft;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface SpaceCraftService {

    SpaceCraft save(SpaceCraft spaceCraft);

    List<SpaceCraft> findAll(Pageable pageable);

    Optional<SpaceCraft> findById(long id);

    List<SpaceCraft> findByNameContainingIgnoreCase(String name, Pageable pageable);

    void remove(long id);

    SpaceCraft update(SpaceCraft spaceCraft);
}
