package com.world2meet.spacecraft.exceptions;

import com.world2meet.spacecraft.model.Error;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class AppExceptionHandler {

    public static final String DATA_INTEGRITY_EXCEPTION = "DATA_INTEGRITY_EXCEPTION";
    public static final String INVALID_PARAMETER_HEADER = "INVALID_PARAMETER_HEADER";
    public static final String NOT_FOUND_EXCEPTION = "NOT_FOUND_EXCEPTION";
    private static final HttpStatus BAD_REQUEST = HttpStatus.BAD_REQUEST;



    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = { DataIntegrityViolationException.class })
    @ResponseBody
    public ResponseEntity dataIntegrityViolationException(DataIntegrityViolationException e) {

        return ResponseEntity.status((HttpStatus.INTERNAL_SERVER_ERROR.value())).body(Error.builder()
                .error(DATA_INTEGRITY_EXCEPTION)
                .message(e.getLocalizedMessage())
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).build());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = { SpaceCraftEntityNotFoundException.class })
    @ResponseBody
    public ResponseEntity spaceCraftEntityNotFoundException(SpaceCraftEntityNotFoundException e) {

        return ResponseEntity.status((HttpStatus.NOT_FOUND.value())).body(Error.builder()
                .error(NOT_FOUND_EXCEPTION)
                .message(e.getLocalizedMessage())
                .statusCode(HttpStatus.NOT_FOUND.value()).build());
    }
}
