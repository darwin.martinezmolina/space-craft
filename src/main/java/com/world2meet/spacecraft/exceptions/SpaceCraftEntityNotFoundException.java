package com.world2meet.spacecraft.exceptions;

public class SpaceCraftEntityNotFoundException extends RuntimeException {
    public SpaceCraftEntityNotFoundException(final String entity, final String field, final String searchParams) {
        super(String.format("%s was not found for parameters %s = %s", entity, field, searchParams));
    }

}
