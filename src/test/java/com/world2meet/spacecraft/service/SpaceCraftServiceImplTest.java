package com.world2meet.spacecraft.service;

import com.world2meet.spacecraft.entities.SpaceCraft;
import com.world2meet.spacecraft.repository.SpaceCraftRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SpaceCraftServiceImplTest {

    @Mock
    private SpaceCraftRepository repository;

    @InjectMocks
    private SpaceCraftServiceImpl service;

    private SpaceCraft spaceCraft;
    private List<SpaceCraft> spaceCraftList;
    private Page<SpaceCraft> spaceCraftPage;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        spaceCraft = new SpaceCraft();
        spaceCraft.setId(1L);
        spaceCraft.setName("Apollo");
        spaceCraftList = Arrays.asList(spaceCraft);
        spaceCraftPage = new PageImpl<>(spaceCraftList);
    }

    @Test
    void testSave() {
        when(repository.save(spaceCraft)).thenReturn(spaceCraft);

        SpaceCraft result = service.save(spaceCraft);
        assertNotNull(result);
        assertEquals(spaceCraft.getName(), result.getName());
        verify(repository, times(1)).save(spaceCraft);
    }

    @Test
    void testFindAll() {
        Pageable pageable = PageRequest.of(0, 5);
        when(repository.findAll(pageable)).thenReturn(spaceCraftPage);

        List<SpaceCraft> result = service.findAll(pageable);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(spaceCraft.getName(), result.get(0).getName());
        verify(repository, times(1)).findAll(pageable);
    }

    @Test
    void testFindById() {
        when(repository.findById(1L)).thenReturn(Optional.of(spaceCraft));

        Optional<SpaceCraft> result = service.findById(1L);
        assertTrue(result.isPresent());
        assertEquals(spaceCraft.getName(), result.get().getName());
        verify(repository, times(1)).findById(1L);
    }

    @Test
    void testFindByNameContainingIgnoreCase() {
        Pageable pageable = PageRequest.of(0, 5);
        when(repository.findByNameContainingIgnoreCase("Apollo", pageable)).thenReturn(spaceCraftPage);

        List<SpaceCraft> result = service.findByNameContainingIgnoreCase("Apollo", pageable);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(spaceCraft.getName(), result.get(0).getName());
        verify(repository, times(1)).findByNameContainingIgnoreCase("Apollo", pageable);
    }

    @Test
    void testRemove() {
        when(repository.findById(1L)).thenReturn(Optional.of(spaceCraft));
        doNothing().when(repository).deleteById(1L);

        service.remove(1L);
        verify(repository, times(1)).findById(1L);
        verify(repository, times(1)).deleteById(1L);
    }
}