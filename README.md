# SpaceCraft Project

## Description
This project is a Spring Boot application that manages SpaceCraft entities. It provides a REST API for creating, updating, deleting, and querying space crafts.

## Requirements
- Java 22
- Maven 3.8.6 or higher
- Docker (optional, if you want to run in a container)

## Installation

### Clone the Repository
First, clone the project repository to your local machine:

```sh
git clone https://gitlab.com/darwin.martinezmolina/space-craft.git
cd space-craft
```

### Configure the Scripts
The project has two scripts in the scripts folder that need execution permissions:

- build.sh: Compiles the project and builds the package.
- setenv.sh: Include some environment variables needed for the installation.

To give execution permissions to the scripts, run the following commands:

```sh
chmod 755 scripts/build.sh
chmod 755 scripts/setenv.sh
```

### Run the Build Script
Once the scripts are configured, execute the build.sh script to compile the project:

```sh
./scripts/build.sh
```

This script will perform the following tasks:

- Clean the project.
- Compile the source code.
- Build docker image
- Run Container App
  
This will start the Spring Boot application on the configured port (default is 8080).

## Swagger Documentation
The API documentation can be accessed using Swagger UI. Visit the following URL in your browser:

[Swagger UI](http://localhost:8080/swagger-ui/index.html#/)

##  Postman Collection
It can import the Postman collection to quickly test the API endpoints. Download the collection from the following link:

[Postman Collection](./src/main/resources/space-craft.postman_collection.json)

## Usage
Once the application is up and running, you can interact with the REST API using tools like curl, or Postman.

Example requests:

- Get all space crafts:

```sh
curl --location 'localhost:8080/v1/spacecraft?page=0&size=5' \
--header 'Cookie: Idea-40180153=ec9cb7c7-d90e-405c-b86f-b49812aa110b; X-CSRF-TOKEN=77ea67f8-23fd-4c3a-a314-2d716fd066b0; amlbcookie=01; i18next=en'
```

- Get a space craft by ID:

```sh
curl --location 'localhost:8080/v1/spacecraft/1' \
--header 'Cookie: Idea-40180153=ec9cb7c7-d90e-405c-b86f-b49812aa110b; X-CSRF-TOKEN=77ea67f8-23fd-4c3a-a314-2d716fd066b0; amlbcookie=01; i18next=en'
```

- Create a new space craft:

```sh
curl --location 'localhost:8080/v1/spacecraft' \
--header 'Content-Type: application/json' \
--header 'Cookie: Idea-40180153=ec9cb7c7-d90e-405c-b86f-b49812aa110b; X-CSRF-TOKEN=77ea67f8-23fd-4c3a-a314-2d716fd066b0; amlbcookie=01; i18next=en' \
--data '{
"name":"space2",
"description": "description Space1"
}'
```

- Update a space craft:

```sh
curl --location --request PUT 'localhost:8080/v1/spacecraft' \
--header 'Content-Type: application/json' \
--header 'Cookie: Idea-40180153=ec9cb7c7-d90e-405c-b86f-b49812aa110b; X-CSRF-TOKEN=77ea67f8-23fd-4c3a-a314-2d716fd066b0; amlbcookie=01; i18next=en' \
--data '{   "id": 1,
"name":"space23333333",
"description": "description Space1"
}'
```

- Delete a space craft:

```sh
curl --location --request DELETE 'localhost:8080/v1/spacecraft/1' \
--header 'Cookie: Idea-40180153=ec9cb7c7-d90e-405c-b86f-b49812aa110b; X-CSRF-TOKEN=77ea67f8-23fd-4c3a-a314-2d716fd066b0; amlbcookie=01; i18next=en'
```

## Notes
Make sure you have all the necessary dependencies installed and configured correctly.
Check that the port configured in the application is not being used by other services on your machine.

## Contributions
Contributions are welcome. Please open an issue or a pull request in the repository.